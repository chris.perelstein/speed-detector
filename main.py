#!/usr/bin/env python

import os
import glob
import shutil
from pathlib import Path
import time
from datetime import datetime, timezone
from zoneinfo import ZoneInfo
import argparse
import json
from multiprocessing import Lock, Pipe, Process
from multiprocessing.shared_memory import SharedMemory
import numpy as np
import cv2
from picamera2 import Picamera2

from vehicle_counter import VehicleCounter

WORKER_COUNT = 3

# Colors for drawing on processed frames
DIVIDER_COLOR = (255, 255, 0)
BOUNDING_BOX_COLOR = (255, 0, 0)
CENTROID_COLOR = (0, 0, 255)

parser = argparse.ArgumentParser()
parser.add_argument("road_name",
                            help="road config from settings.json")
parser.add_argument("-t", "--token",
                            help="token for publishing to influxdb")
parser.add_argument("-v", "--verbose", action="count", default=0,
                            help="verbosity level")
args = parser.parse_args()

with open('settings.json') as f:
    data = json.load(f)

    try:
        ROAD = data[args.road_name]
    except KeyError:
        raise Exception('Road name not recognized.')

# For cropped rectangles
ref_points = []
ref_rects = []

def click_and_crop (event, x, y, flags, param):
    global ref_points

    if event == cv2.EVENT_LBUTTONDOWN:
        ref_points = [(x, y)]

    elif event == cv2.EVENT_LBUTTONUP:
        (x1, y1), x2, y2 = ref_points[0], x, y
        ref_points[0] = (min(x1, x2), min(y1, y2))
        ref_points.append((max(x1, x2), max(y1, y2)))
        ref_rects.append((ref_points[0], ref_points[1]))

# Write cropped rectangles to file for later use/loading
def save_cropped():
    global ref_rects

    with open('settings.json', 'r+') as f:
        data = json.load(f)
        data[args.road_name]['cropped_rects'] = ref_rects

        f.seek(0)
        json.dump(data, f, indent=4)
        f.truncate()

    print('Saved ref_rects to settings.json!')

# Load any saved cropped rectangles
def load_cropped ():
    global ref_rects

    ref_rects = ROAD['cropped_rects']

    print('Loaded ref_rects from settings.json!')

# Remove cropped regions from frame
def remove_cropped (frame):
    for rect in ref_rects:
        frame[ rect[0][1]:rect[1][1], rect[0][0]:rect[1][0] ] = 0
        #cropped[ rect[0][1]:rect[1][1], rect[0][0]:rect[1][0] ] = (0,0,0)

    return frame

def get_centroid (x, y, w, h):
    x1 = w // 2
    y1 = h // 2

    return(x + x1, y + y1)

def vehicle_worker(specs, display_lock, display_pipe, worker_pipe):
    car_counter = VehicleCounter(ROAD, args.verbose, args.token)

    frame_shm = []
    frame = []
    mask_shm = []
    mask = []

    for i in range(WORKER_COUNT):
        frame_shm.append(SharedMemory(f"frame{i}"))
        frame.append(np.ndarray(specs['frame_shape'], specs['frame_dtype'], buffer=frame_shm[i].buf))
        mask_shm.append(SharedMemory(f"mask{i}"))
        mask.append(np.ndarray(specs['gray_shape'], specs['gray_dtype'], buffer=mask_shm[i].buf))

    try:
        while True:
            for i in range(WORKER_COUNT):
                frametime, exposure, matches = worker_pipe[i].recv()

                #tuple with fastest,starttime,array of new vehicles
                fastest, distance_feet, start_time, start_vehicle = car_counter.update_count(matches, frametime, exposure, frame[i])

                #mark and store start frame of vehicle in memory
                if start_vehicle:
                    capfile = f"/dev/shm/{frametime}.jpg"
                    display_time = datetime.fromtimestamp(frametime, ZoneInfo('America/New_York')).isoformat(timespec='milliseconds')
                    cv2.putText(frame[i], display_time, (10, 230), cv2.FONT_HERSHEY_PLAIN, 2, (127, 255, 255), 2)
                    cv2.imwrite(capfile, cv2.copyMakeBorder(frame[i], 300, 300, 0, 0, cv2.BORDER_CONSTANT, 0))
                    cv2.putText(frame[i], display_time, (10, 230), cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 0), 2)

                #if over a threshold, mark and store final frame to disk
                #plus move corresponding start frame from memory to disk
                if fastest > 55.0:
                    seconds = round(frametime - start_time, 2)

                    #timestamps for display/filename
                    display_time = datetime.fromtimestamp(frametime, ZoneInfo('America/New_York')).isoformat(timespec='milliseconds')
                    file_time = datetime.fromtimestamp(frametime, ZoneInfo('America/New_York')).isoformat(timespec='seconds')

                    #mv start image out of memory
                    shutil.copy(f"/dev/shm/{start_time}.jpg", f"capture/{file_time}_{fastest}_begin.jpg")

                    #finish image with text
                    capfile = f"capture/{file_time}_{fastest}_end.jpg"
                    cv2.putText(frame[i], display_time, (10, 230), cv2.FONT_HERSHEY_PLAIN, 2, (127, 255, 255), 2)
                    finish_img = cv2.copyMakeBorder(frame[i], 300, 300, 0, 0, cv2.BORDER_CONSTANT, 0)
                    accuse = f'{int(fastest)}MPH!!! {distance_feet}ft over {seconds} seconds'
                    cv2.putText(finish_img, accuse, (10, 700), cv2.FONT_HERSHEY_PLAIN, 3, (127, 255, 255), 2)
                    cv2.imwrite(capfile, finish_img)

                    #trigger tweet
                    Path(f'tweet/{file_time}_{fastest}').touch()

                #clean up stale start frames from memory
                for file in glob.glob('/dev/shm/*.jpg'):
                    file_time = os.stat(file).st_mtime
                    if (file_time < frametime - 10):
                        os.remove(file)

                #do display
                if i == 0 and os.path.isfile('config/show'):
                    if display_pipe.poll() and display_lock.acquire(block=False):
                        display_pipe.recv()
                        display_pipe.send((frame[i],mask[i]))
                        display_lock.release()

                worker_pipe[i].send('done')
    except KeyboardInterrupt:
        for i in range(WORKER_COUNT):
            frame_shm[i].close()
            mask_shm[i].close()
        print('goodbye from counter_worker')

def display_worker(display_lock, display_pipe):
    cv2.namedWindow('Processed Image', cv2.WINDOW_NORMAL)
    cv2.namedWindow('Filtered Mask', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('Processed Image', 1000, 300)
    cv2.resizeWindow('Filtered Mask', 1000, 300)
    cv2.moveWindow('Processed Image', 10, 10)
    cv2.moveWindow('Filtered Mask', 10, 500)

    try:
        while True:
            display_pipe.send('ready')
            frame,mask=display_pipe.recv()
            display_lock.acquire()

            cv2.imshow('Processed Image', frame)
            cv2.imshow('Filtered Mask', mask)

            cv2.waitKey(1)
            display_lock.release()
    except KeyboardInterrupt:
        display_pipe.close()
        print('goodbye from display_worker')

def frame_worker(worker_id, specs, parent_pipe, vehicle_pipe):
    bg_subtractor = cv2.createBackgroundSubtractorKNN(history=200, dist2Threshold=700.0, detectShadows=True)
    #bg_subtractor = cv2.createBackgroundSubtractorKNN(history=500, dist2Threshold=400.0, detectShadows=True)
    #bg_subtractor = cv2.createBackgroundSubtractorMOG2(history=600, varThreshold=21, detectShadows=True)
    bg_subtractor.setShadowValue(0)

    divider_horizontal=False
    divider=1000

    MIN_CONTOUR_WIDTH = 33
    MIN_CONTOUR_HEIGHT = 21
    KERNEL_OPEN = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (4, 4))
    KERNEL_CLOSE = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (9, 9))

    frame_shm = SharedMemory(f"frame{worker_id}")
    mask_shm = SharedMemory(f"mask{worker_id}")
    frame = np.ndarray(specs['frame_shape'], specs['frame_dtype'], buffer=frame_shm.buf)
    mask = np.ndarray(specs['gray_shape'], specs['gray_dtype'], buffer=mask_shm.buf)

    try:
        while True:
            frametime, exposure = parent_pipe.recv()

            frame[:] = cv2.GaussianBlur(frame,(5,5),0)
            frame[:] = remove_cropped(frame)

            mask[:] = bg_subtractor.apply(frame)

            #process images and find countours
            #mask[:] = cv2.morphologyEx(mask, cv2.MORPH_OPEN, KERNEL_OPEN)
            mask[:] = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, KERNEL_CLOSE, iterations=4)
            #mask[:] = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, KERNEL_CLOSE)
            #frame = cv2.morphologyEx(frame, cv2.MORPH_OPEN, KERNEL_OPEN)
            #ret, frame = cv2.threshold(frame, 227, 255, cv2.THRESH_BINARY)
            contours, hierarchy = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

            matches = []

            #check if it could be a vehicle, mark perimeter and center
            for (i, contour) in enumerate(contours):
                x, y, w, h = cv2.boundingRect(contour)
                contour_valid = (w >= MIN_CONTOUR_WIDTH) and (h >= MIN_CONTOUR_HEIGHT)

                if not contour_valid or not hierarchy[0,i,3] == -1:
                    continue

                centroid = get_centroid(x, y, w, h)

                matches.append(((x, y, w, h), centroid))
                cv2.rectangle(frame, (x, y), (x + w - 1, y + h - 1), BOUNDING_BOX_COLOR, 1)
                cv2.circle(frame, centroid, 2, CENTROID_COLOR, -1)

            if divider_horizontal:
                cv2.line(frame, (0, divider), (frame.shape[1], divider), DIVIDER_COLOR, 1)
            else:
                cv2.line(frame, (divider, 0), (divider, frame.shape[0]), DIVIDER_COLOR, 1)

            #signal carcounter
            vehicle_pipe.send((frametime, exposure, matches))
            vehicle_pipe.recv()

            parent_pipe.send(frametime)
    except KeyboardInterrupt:
        frame_shm.close()
        mask_shm.close()
        frame_shm.unlink()
        mask_shm.unlink()
        print('goodbye from worker')

def main ():
    load_cropped()

    picam2 = Picamera2()
    config = picam2.create_still_configuration(main={"size": (1152, 238), "format": "RGB888"}, raw={"size": (2304, 1296)}, buffer_count=4)
    picam2.configure(config)
    #picam2.set_controls({"ExposureTime": 10000, "AnalogueGain": 25.0})
    #picam2.set_controls({"FrameRate": 25.0})
    picam2.set_controls({"ScalerCrop": (0, 800, 4608, 952)})
    #picam2.set_controls({"Saturation": 0})
    picam2.start()
    frame = picam2.capture_array("main")
    mask = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    specs = {
            'frame_shape': frame.shape,
            'frame_dtype': frame.dtype,
            'frame_nbytes': frame.nbytes,
            'gray_shape': mask.shape,
            'gray_dtype': mask.dtype,
            'gray_nbytes': mask.nbytes,
            }

    worker = [None] * WORKER_COUNT
    worker_vehicle_pipe = [None] * WORKER_COUNT
    worker_process = []
    worker_image_shm = []
    worker_mask_shm = []
    framebuffer = []

    display_lock = Lock()
    display_pipe, vehicle_display_pipe = Pipe(True)

    #set up display process for displaying images for testing
    display_process = Process(target=display_worker, args=(display_lock, display_pipe))
    display_process.start()

    #set up worker process and seed with initial frame
    for i in range(WORKER_COUNT):
        request = picam2.capture_request()
        frame = request.make_array("main")
        metadata = request.get_metadata()
        request.release()

        frametime = time.time()
        worker_image_shm.append(SharedMemory(name=f"frame{i}", create=True, size=specs['frame_nbytes']))
        worker_mask_shm.append(SharedMemory(name=f"mask{i}", create=True, size=specs['gray_nbytes']))
        framebuffer.append(np.ndarray(specs['frame_shape'], specs['frame_dtype'], buffer=worker_image_shm[i].buf))
        framebuffer[i][:] = frame

        worker[i], worker_pipe = Pipe(True)
        worker_vehicle_pipe[i], vehicle_pipe = Pipe(True)
        worker_process.append(Process(target=frame_worker, args=(i, specs, worker_pipe, vehicle_pipe)))
        worker_process[i].start()

        worker[i].send((frametime, metadata['ExposureTime']))

    vehicle_process = Process(target=vehicle_worker, args=(specs, display_lock, vehicle_display_pipe, worker_vehicle_pipe))
    vehicle_process.start()

    current_worker = 0

    try:
        while True:
            worker[current_worker].recv()

            #framebuffer[current_worker][:] = picam2.capture_array("main")

            request = picam2.capture_request()
            framebuffer[current_worker][:] = request.make_array("main")
            metadata = request.get_metadata()
            request.release()

            worker[current_worker].send((time.time(), metadata['ExposureTime']))

            current_worker += 1
            if current_worker == WORKER_COUNT:
                current_worker = 0

    except KeyboardInterrupt:
        vehicle_process.join()
        display_process.join()
        for worker in worker_process:
            worker.join()
        for i,m in zip(worker_image_shm, worker_mask_shm):
            i.close()
            i.unlink()
            m.close()
            m.unlink()
        print("GOODBYE")

if __name__ == '__main__':
    main()
