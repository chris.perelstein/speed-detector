import math
import time
import json
from datetime import timezone, datetime
import cv2
import numpy as np
from influxdb_client import InfluxDBClient, Point
from influxdb_client .client.write_api import SYNCHRONOUS

# https://stackoverflow.com/questions/36254452/counting-cars-opencv-python-issue/36274515#36274515

class Vehicle (object):
    def __init__ (self, carid, position, startframetime):
        self.id = carid
        
        self.positions = [position]
        self.frames_since_seen = 0
        self.counted = False
        self.startframetime = startframetime

        self.speed = None

        # assign a random color for the car
        self.color = (np.random.randint(255), np.random.randint(255), np.random.randint(255))

    @property
    def last_position (self):
        return self.positions[-1]
    
    def add_position (self, new_position):
        self.positions.append(new_position)
        self.frames_since_seen = 0

    def draw (self, output_image):
        for point in self.positions:
            cv2.circle(output_image, point, 4, self.color, -1)
            cv2.polylines(output_image, [np.int32(self.positions)], False, self.color, 1)

        if self.speed:
            cv2.putText(output_image, ("%1.2f" % self.speed), self.last_position, cv2.FONT_HERSHEY_PLAIN, 1.5, (127, 255, 255), 1)


class VehicleCounter (object):
    def __init__(self, road, verbose=0, token=None):
        self.divider = road['divider']
        self.is_horizontal = road['divider_horizontal']
        self.pass_side = road['divider_pass_side']
        self.vector_angle_min = road['vector_angle_min']
        self.vector_angle_max = road['vector_angle_max']
        self.vehicles = []
        self.next_vehicle_id = 0
        self.max_unseen_frames = 15
        self.distance = road['distance']
        self.client = None
        # this needs to be moved to road config
        self.speed_factor = 8.8

        if token:
            self.client = InfluxDBClient(url='http://localhost:8086', org='traffic', token=token, database='traffic')
            self.write_api = self.client.write_api(write_options=SYNCHRONOUS)

    @staticmethod
    def get_vector (a, b):
        dx = float(b[0] - a[0])
        dy = float(b[1] - a[1])

        distance = math.sqrt(dx**2 + dy**2)

        if dy > 0:
            angle = math.degrees(math.atan(-dx/dy))
        elif dy == 0:
            if dx < 0:
                angle = 90.0
            elif dx > 0:
                angle = -90.0
            else:
                angle = 0.0
        else:
            if dx < 0:
                angle = 180 - math.degrees(math.atan(dx/dy))
            elif dx > 0:
                angle = -180 - math.degrees(math.atan(dx/dy))
            else:
                angle = 180.0

        return distance, angle

    @staticmethod
    def is_valid_vector (a, angle_min, angle_max):
        # TODO!
        distance, angle = a
        # TODO: This also needs to be customized!
        return (distance <= 160 and angle > angle_min and angle < angle_max)

    # This method should be customizeable/depend on external settings (i.e. horizontal vs vertical divider)
    # see explanation for pass_side in settings.json
    def is_past_divider (self, centroid):
        x, y = centroid

        if self.is_horizontal:
            if self.pass_side == -1:
                return y < self.divider
            else:
                return y > self.divider

        else:
            if self.pass_side == -1:
                return x < self.divider
            else:
                return x > self.divider


    def update_vehicle (self, vehicle, matches):
        best = None

        # Find if any of the matches fits this vehicle
        for i, match in enumerate(matches):
            contour, centroid = match
            x, y, w, h = contour

            vector = self.get_vector(vehicle.last_position, (x + w - 1, centroid[1]))
            if self.is_valid_vector(vector, self.vector_angle_min, self.vector_angle_max):
                #print('Angle: %s' % vector[1])
                if not best or vector[0] < best[0]:
                    best = (vector[0], i, (x + w - 1, centroid[1]))

                #vehicle.add_position((x + w - 1, centroid[1]))

                #return i

        if best:
            vehicle.add_position(best[2])

            return best[1]


        # No matches fit
        # print('No matches found for vehicle %s' % vehicle.id)
        vehicle.frames_since_seen += 1

        return None


    # TODO: REMOVE small "ghost tracks" by comparing them to avg distance!!!
    def update_count (self, matches, frametime, exposure, output_image=None):
        fastest=0
        distance_feet=0
        start_time=0
        start_vehicle=False

        # Update existing vehicles
        for vehicle in self.vehicles:
            i = self.update_vehicle(vehicle, matches)
            if i is not None:
                del matches[i]

        # For remaining matches, add new vehicles
        # TODO: IMPORTANT: the bug of multiple little tracks on the same vehicle FIX THIS RONIT GODDAMN IT!!!
        for match in matches:
            contour, centroid = match
            x,y,w,h = contour
            
            # skip_this = False

            # for v in self.vehicles:
            # 	if v.last_position == centroid:
            # 		skip_this = True
            # 		break

            # if skip_this:
            # 	continue

            if not self.is_past_divider(centroid) and x+w<110:
                new_vehicle = Vehicle(self.next_vehicle_id, (x + w - 1, centroid[1]), frametime)
                self.next_vehicle_id += 1
                self.vehicles.append(new_vehicle)
                start_vehicle=True

        # Count any uncounted vehicles that are past the divider
        for vehicle in self.vehicles:
            if not vehicle.counted and self.is_past_divider(vehicle.last_position):
                if exposure > 49000:
                    print(f"reject exposure: {exposure}")
                    vehicle.counted = True
                    continue

                distance = self.get_vector(vehicle.last_position, vehicle.positions[0])[0] # We don't need the angle

                total_time = frametime - vehicle.startframetime

                print(f"IX: {vehicle.positions[0][0]}, EX: {exposure}, VLENGTH: {distance}, VTIME: {total_time}, VSPEED: {distance/total_time}")

                vehicle.counted = True

                # MPH
                vehicle.speed = round(distance / (self.speed_factor * total_time), 2)

                if vehicle.speed > fastest:
                    fastest = vehicle.speed
                    start_time = vehicle.startframetime
                    distance_feet = round((22 * distance) / (15 * self.speed_factor), 2)

                isotime = datetime.fromtimestamp(int(frametime),tz=timezone.utc).isoformat()
                data_body = f'[{{"measurement": "speed", "time": "{isotime}", "fields": {{"value": {vehicle.speed}}}}}]'
                #data_body = f'speed value={vehicle.speed} {time.time()}'
                print(data_body)

                if self.client:
                    point = Point('speed').field('value', vehicle.speed).time(isotime)
                    self.write_api.write(bucket="traffic", record=[point])

        # Draw the vehicles (optional)
        if output_image is not None:
            for vehicle in self.vehicles:
                vehicle.draw(output_image)

        # Remove vehicles that have not been seen in a while
        removed = [v.id for v in self.vehicles
            if v.frames_since_seen >= self.max_unseen_frames]
        self.vehicles[:] = [v for v in self.vehicles
            if not v.frames_since_seen >= self.max_unseen_frames]
        # for carid in removed:
        # 	print('Removed vehicle', carid)

        # print('Count updated')
        return (fastest, distance_feet, start_time, start_vehicle)
