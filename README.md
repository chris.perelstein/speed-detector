# Python/OpenCV Speed Detector #

Forked from: [https://github.com/ronitsinha/speed-detector/](https://github.com/ronitsinha/speed-detector/).

This is a program that uses OpenCV to calculate vehicle speeds based on images from a camera. Currently, this is fairly hardcoded for a raspberry pi and a picamera.

### How it works ###

I've only made small modifications to the original underlying method, so mostly just updating the following sections. One thing I want to call out is I got a quite a bit better performance in terms of frames processed per second by using multi-processing, but this involved a bunch of shenanigans related to storing things in memory and sending messages around on pipes. I don't have a huge amount of experience with this so I may have done it suboptimally.

#### Cropping ####

I removed the bulk of the cropping code to focus on reducing the image I'm working with. I trim as much area via picamera config to reduce the size. This reduces the size of data I'm manipulating or passing around, as well as reduces the need for much cropping by blanking out areas. I still blank out sidewalks and trees to reduce noise in image change, but also a crude attempt at preserving privacy for those on the sidewalks.

#### Vehicle Detection ####

Now that the unwanted areas are removed, we can use computer vision to isolate the vehicles (after all, that's what we really care about!). 

I use KNN background subtraction and morphology to isolate the vehicles and detect their contours. I'm not going to explain too much since these are default OpenCV functions, but you can see how I use them in the first part of `process_frame()` and `filter_mask()` in `main.py`.

#### Vehicle Tracking ####

Vehicles are tracked by ensuring we are looking at top level contour of the expected size, and making sure the vector for each new position is reasonable and best fit. If so, each position is added as appropriate to the tracked vehicle. As a sanity check, a track is drawn over the image which get's stored on the final frame that's captured when the speed is super high.

#### Speed Calculation ####

I diverge here a bit from Ronit's original code. I'm wanting to get as precise of a speed number as possible so I instead calculate a vector based on the first and last position of a vehicle, then use the time of the captured frames to determine speed. There is a speed factor that I have to apply to convert this to a real world mph, so I calculated that initially using a helper driving at multiple set speeds through the target area and doing some math. After a while of observing real world data, I was concerned at the high numbers I was getting and suspected an error or bug, so I started capturing start and finish images for each vehicle. I then used a measuring wheel to measure the actual observed distance a vehicle went and used that to verify my data. Turns out it's still pretty darn close, maybe ±2mph. I suspect at the faster speeds there may be a little more error but not enough to impact both broad conclusions about traffic, nor the fact that extreme speeders are infact speeding extremely. 

#### Settings ####

I've modified enough of this that the settings file unfortunately isn't the whole picture anymore. I really need to refactor this to make it more generic again.

I designed this program so that it could work on virtually any video source.

`settings.json` stores settings for each individual video source (I call them "roads" in my program). A few examples include the positon of the detection line, the url of the video source, and the cropped out regions. Of course, you can look at `settings.json` to see how I actually store these values.

### Improvements ###

* Refactor to be properly configurable again.
* Better camera? I used a noir picamera hence the weird colors. I was hoping to improve performance at dusk/night but this didn't really work out so I ignore things once we go below a certain exposure level.
* Switch to some kind of AI/ML object detection to improve robustness. I end up losing an occasion vehicle when they are too close, or when shadows/glare cause detection issues. Ideally this could solve my night time problem as well.
* Better hardware for redployment to other locations.
